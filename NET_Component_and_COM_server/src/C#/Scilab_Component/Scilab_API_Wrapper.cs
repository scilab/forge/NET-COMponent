﻿/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan CORNET
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
//=============================================================================
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Forms;
//=============================================================================
namespace Scilab_Component
{
    public class Scilab_API_Wrapper
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public unsafe struct api_Ctx
        {
            public String pstName; /**< Function name */
        }
        //=============================================================================
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public unsafe struct SciErr
        {
            public int iErr;
            public int iMsgCount;
            public fixed int pstructMsg[5];
        }
        //=============================================================================
        private const string API_SCILAB_DLL = "api_scilab.dll";
        private const string GRAPHICS_DLL = "graphics.dll";
        private const string OUTPUT_STREAM_DLL = "output_stream.dll";
        private const string SCILAB_WINDOWS_DLL = "scilab_windows.dll";
        private const string ACTION_BINDING_DLL = "sciaction_binding.dll";
        private const string LIBSCILAB_DLL = "libscilab.dll";
        private const string CONSOLE_DLL = "sciconsole.dll";
        private const string GUI_DLL = "scigui.dll";
        private const string FILE_SEPARATOR = "\\";
        //=============================================================================
        public static Boolean checkDllsUsed(string pathScilabBin)
        {

            Boolean bOK = checkDllUsed(pathScilabBin, API_SCILAB_DLL);

            if (bOK) bOK = checkDllUsed(pathScilabBin, GRAPHICS_DLL);
            if (bOK) bOK = checkDllUsed(pathScilabBin, OUTPUT_STREAM_DLL);
            if (bOK) bOK = checkDllUsed(pathScilabBin, SCILAB_WINDOWS_DLL);
            if (bOK) bOK = checkDllUsed(pathScilabBin, ACTION_BINDING_DLL);
            if (bOK) bOK = checkDllUsed(pathScilabBin, LIBSCILAB_DLL);
            if (bOK) bOK = checkDllUsed(pathScilabBin, CONSOLE_DLL);
            if (bOK) bOK = checkDllUsed(pathScilabBin, GUI_DLL);

            return bOK;
        }
        //=============================================================================
        private static  Boolean checkDllUsed(string pathScilabBin, string dllName)
        {
            Boolean bOK = File.Exists(pathScilabBin + FILE_SEPARATOR + dllName);
            if (!bOK)
            {
                MessageBox.Show(dllName + "not found.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop); 
            }
            return bOK;
        }
        //=============================================================================
        [DllImport(GUI_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isVisibleMainWindow();
        [DllImport(GUI_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern void setVisibleMainWindow([In]int bVisibleState);
        //=============================================================================
        [DllImport(LIBSCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isEmptyCommandQueue();
        //=============================================================================
        [DllImport(LIBSCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int StoreCommandWithFlag([In]string command, Int32 flag);
         //=============================================================================
        [DllImport(SCILAB_WINDOWS_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int Windows_Main([In]System.Int32 hInstance,
          [In]System.Int32 hPrevInstance,
          [In]string szCmdLine, Int32 iCmdShow);
        //=============================================================================
        [DllImport(CONSOLE_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int ConsoleIsWaitingForInput();
        //=============================================================================
        [DllImport(GRAPHICS_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int sciHasFigures();
        //=============================================================================
        [DllImport(OUTPUT_STREAM_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int GetLastErrorCode();
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfString([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [In] int _iRows, [In] int _iCols,
            [In] String[] _pstStrings);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfString([In]IntPtr _pvCtx,
            [In] String _pstName,
            [Out]  Int32* _piRows, [Out]  Int32* _piCols,
            [In, Out] int[] _piLength,
            [In, Out] String[] _pstStrings);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedStringType([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedSingleString([In]IntPtr _pvCtx,
            [In] String _pstName,
            [In] String _pstStrings);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr getNamedVarType([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [Out]Int32* _piType);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedVarComplex([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr getNamedVarDimension([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int32* _piRows, [Out] Int32* _piCols);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedVarMatrixType([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedEmptyMatrix([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedVarExist([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedRowVector([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedColumnVector([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedVector([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedScalar([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedSquareMatrix([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedEmptyMatrix([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfBoolean([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] int _iRows, [In] int _iCols, 
            [In] int[] _piBool);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfBoolean([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [Out] Int32* _piRows, [Out] Int32* _piCols, 
            [Out] int[] _piBool);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedBooleanType([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarBoolean([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [Out] Int32* _piBool);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarBoolean([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            int _iBool);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedPointerType([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfDouble([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [In] Int32 _iRows, [In] Int32 _iCols,
            [In] double[] _pdblReal);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedComplexMatrixOfDouble([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [In] Int32 _iRows, [In] Int32 _iCols,
            [In] double[] _pdblReal, [In] double[] _pdblImg);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfDouble([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [Out] Int32* _piRows, [Out] Int32* _piCols,
            [In, Out] Double[] _pdblReal);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedComplexMatrixOfDouble([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int32* _piRows, [Out] Int32* _piCols,
            [In, Out] Double[] _pdblReal, [In, Out] Double[] _pdblImg);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedDoubleType([In]IntPtr _pvCtx,
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarDouble([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [Out] Double * _pdblReal);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarComplexDouble([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [Out] Double* _pdblReal, [Out] Double* _pdblImg);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarDouble([In]IntPtr _pvCtx,
            [In] String _pstName,
            [In]double _dblReal);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarComplexDouble([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [In] double _dblReal, [In]double _dblImg);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfInteger8([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [In] Int32 _iRows, [In] Int32 _iCols,
            [In] sbyte[] _pcData8);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfUnsignedInteger8([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] int _iRows, [In] int _iCols, 
            [In] byte [] _pucData8);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfInteger16([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [In] int _iRows, [In] int _iCols,
            [In] Int16[] _psData16);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfUnsignedInteger16([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] int _iRows, [In] int _iCols, 
            [In] UInt16 [] _pusData16);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfInteger32([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [In] Int32 _iRows, [In]Int32 _iCols,
            [In] Int32[] _piData32);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr createNamedMatrixOfUnsignedInteger32([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] int _iRows, [In] int _iCols, 
            [In] UInt32[] _puiData32);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr getNamedMatrixOfIntegerPrecision([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int32* _piPrecision);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfInteger8([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int32* _piRows, 
            [Out] Int32* _piCols, 
            [Out] sbyte[] _pcData8);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfUnsignedInteger8([In]IntPtr _pvCtx, 
            [In] String _pstName,
            [Out] Int32* _piRows, [Out] Int32* _piCols, 
            [Out] byte [] _pucData8);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfInteger16([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int32* _piRows, [Out] Int32* _piCols, 
            [Out] Int16[] _psData16);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfUnsignedInteger16([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int32* _piRows, [Out] Int32* _piCols, 
            [Out] ushort[] _pusData16);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfInteger32([In]IntPtr _pvCtx,
            [In] String _pstName,
            [Out] Int32* _piRows, [Out] Int32* _piCols,
            [Out] Int32[] _piData32);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern SciErr readNamedMatrixOfUnsignedInteger32([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int32* _piRows, [Out] Int32* _piCols, 
            [Out] UInt32[] _puiData32);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedIntegerType([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarInteger8([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] sbyte* _pcData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarInteger16([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int16* _psData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarInteger32([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] Int32* _piData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarUnsignedInteger8([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] byte* _pucData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarUnsignedInteger16([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] UInt16* _pusData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int getNamedScalarUnsignedInteger32([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [Out] UInt32* _puiData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarInteger8([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] sbyte _cData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarInteger16([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] Int16 _sData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarInteger32([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] Int32 _iData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarUnsignedInteger8([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] byte _ucData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarUnsignedInteger16([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] UInt16 _usData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int createNamedScalarUnsignedInteger32([In]IntPtr _pvCtx, 
            [In] String _pstName, 
            [In] UInt32 _uiData);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        public unsafe static extern int isNamedBooleanSparseType([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
        [DllImport(API_SCILAB_DLL, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Struct)]
        public unsafe static extern int isNamedPolyType([In]IntPtr _pvCtx, 
            [In] String _pstName);
        //=============================================================================
    }
}
//=============================================================================
