﻿/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan CORNET
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
//=============================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
//=============================================================================
namespace Scilab_Component
{
    /* Scilab Types */
    public enum ScilabType
    {
        sci_matrix = 1,
        sci_poly = 2,
        sci_boolean = 4,
        sci_sparse = 5,
        sci_boolean_sparse = 6,
        sci_matlab_sparse = 7,
        sci_ints = 8,
        sci_handles = 9,
        sci_strings = 10,
        sci_u_function = 11,
        sci_c_function = 13,
        sci_lib = 14,
        sci_list = 15,
        sci_tlist = 16,
        sci_mlist = 17,
        sci_pointer = 128,
        sci_implicit_poly = 129,
        sci_intrinsic_function = 130
    };

    public sealed class Scilab_API
    {
        //=============================================================================
        static Scilab_API instance = null;
        static readonly object padlock = new object();
        Thread scilabThread;
        //=============================================================================
        /// <summary>
        /// Constructor, initialize scilab engine.
        /// </summary>
        public Scilab_API()
        {
            string ScilabPath = FindScilab.getScilabPath();
            if (ScilabPath.CompareTo("") != 0)
            {
                AddScilabPathEnvironment.Add(ScilabPath + "\\bin");
            }

            if (!Scilab_API_Wrapper.checkDllsUsed(ScilabPath + "\\bin")) return;

            scilabThread = new Thread(new ThreadStart(ThreadScilabLoop));

            // start scilab Thread
            scilabThread.Start();
            // We wait scilab
            Thread.Sleep(2000);
            waitEmptyCommandQueue();
            Thread.Sleep(3000);
            // ConsoleIsWaitingForInput broken ???
            //while (Scilab_API_Wrapper.ConsoleIsWaitingForInput() != 1)
            {
                Thread.Sleep(10);
            }
            setVisibleMainWindow(false);
            
        }
        //=============================================================================
        /// <summary>
        /// start Scilab
        /// </summary>
        private static void ThreadScilabLoop()
        {
            int SHOW_HIDE = 0;
            Scilab_API_Wrapper.Windows_Main(0, 0, "", SHOW_HIDE);
        }
        //=============================================================================
        /// <summary>
        /// Singleton 
        /// Only one instance of Scilab_API can be launch
        /// thread safe
        /// </summary>
        public static Scilab_API Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Scilab_API();
                    }
                    return instance;
                }
            }
        }
        //=============================================================================
        /// <summary>
        /// Destructor
        /// </summary>
        ~Scilab_API()
        {
            scilabThread.Abort();
        }
        //=============================================================================
        public void setVisibleMainWindow(Boolean newState)
        {
            if (newState)
            {
                Scilab_API_Wrapper.setVisibleMainWindow(1);
            }
            else
            {
                Scilab_API_Wrapper.setVisibleMainWindow(0);
            }
        }
        //=============================================================================
        public Boolean isVisibleMainWindow()
        {
            return (Scilab_API_Wrapper.isVisibleMainWindow() == 1 ? true : false);
        }
        //=============================================================================
        public void clearAllVariables()
        {
            sendScilabJob("clear");
        }
        //=============================================================================
        public Boolean clearVariable(string varName)
        {
            if ( isNamedVarExist(varName) )
            {
                sendScilabJob("clear " + varName);
                return (isNamedVarExist(varName) == false);
            }
            return false;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scriptFilename"></param>
        /// <returns></returns>
        public int executeScript(string scriptFilename)
        {
            string EXEC_STRING = "Err_executeScript = exec('" + scriptFilename + "','errcatch');";
            int ierr = sendScilabJob(EXEC_STRING);
     
            if (ierr == 0)
            {
                if (isNamedVarExist("Err_executeScript"))
                {
                    ierr = (int)getNamedScalarDouble("Err_executeScript");

                    Scilab_API_Wrapper.StoreCommandWithFlag("clear Err_executeScript", 1);
                    waitEmptyCommandQueue();
                }
            }

            return ierr;
        }
        //=============================================================================
        /// <summary>
        /// Send a job to scilab
        /// </summary>
        /// <param name="command">command to send to scilab</param>
        /// <returns>error code operation, 0 : OK</returns>
        public int sendScilabJob(string command)
        {
            int ierr = -1;

            string COMMAND_EXECSTR = 
                "Err_SendScilabJob = execstr(TMP_EXEC_STRING,\"errcatch\", \"n\");";

            if (createNamedSingleString("TMP_EXEC_STRING", command))
            {
                Scilab_API_Wrapper.StoreCommandWithFlag(COMMAND_EXECSTR, 1);
                waitEmptyCommandQueue();

                Scilab_API_Wrapper.StoreCommandWithFlag("clear TMP_EXEC_STRING", 1);
                waitEmptyCommandQueue();

                if (isNamedVarExist("Err_SendScilabJob"))
                {
                    ierr = (int)getNamedScalarDouble("Err_SendScilabJob");

                    Scilab_API_Wrapper.StoreCommandWithFlag("clear Err_SendScilabJob", 1);
                    waitEmptyCommandQueue();
                }
            }

            return ierr;
        }
        //=============================================================================
        /// <summary>
        /// wait scilab command queue
        /// </summary>
        private void waitEmptyCommandQueue()
        {
            while (Scilab_API_Wrapper.isEmptyCommandQueue() != 1)
            {
                Thread.Sleep(100);
            }
        }
        //=============================================================================
        /// <summary>
        /// get last error code
        /// </summary>
        /// <returns>last error code</returns>
        public int getLastErrorCode()
        {
            return Scilab_API_Wrapper.GetLastErrorCode();
        }
        //=============================================================================
        /// <summary>
        /// Detect if a Scilab graphic window is opened
        /// </summary>
        /// <returns>true or false</returns>
        public Boolean haveGraphicWindows()
        {
            return (Scilab_API_Wrapper.sciHasFigures() == 1);
        }
        //=============================================================================
        /// <summary>
        /// Get dimensions of a named matrix in scilab
        /// </summary>
        /// <returns>a int array. 
        /// if variable name does not exist dimensions are null </returns>
        public unsafe int[] getNamedVarDimension(string matrixName)
        {
            int[] iDim = null;
            int iRows = 0;
            int iCols = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr = 
                Scilab_API_Wrapper.getNamedVarDimension(
                    ptrEmpty,
                    matrixName,
                    &iRows, &iCols);

            if (SciErr.iErr == 0)
            {
                iDim = new int[2];
                iDim[0] = iRows;
                iDim[1] = iCols;
            }
            return iDim;
        }
        //=============================================================================
        /// <summary>
        /// Write a named matrix of string in scilab
        /// </summary>
        /// <param name="matrixName"> variable name</param>
        /// <param name="iRows"> Number of row</param>
        /// <param name="iCols"> Number of column</param>
        /// <param name="matrixDouble"> pointer on data</param>
        /// <returns> if the operation successes (0) or not ( !0 )</returns>
        public int createNamedMatrixOfString(string matrixName,
            int iRows, int iCols,
            string[] matrixString)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.createNamedMatrixOfString(ptrEmpty,
                matrixName,
                iRows, iCols,
                matrixString);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// Read a named matrix of string from scilab
        /// </summary>
        /// <param name="matrixName"> variable name</param>
        /// <returns>a matrix of string from scilab. If Variable name does not exist returns null</returns>
        public unsafe string[] readNamedMatrixOfString(string matrixName)
        {
            string[] matrixString = null;

            int[] iDim = getNamedVarDimension(matrixName);

            if (iDim != null)
            {
                int iRows = iDim[0];
                int iCols = iDim[1];

                // we allocate lengthmatrixString
                int[] lengthmatrixString = new int[iRows * iCols];

                System.IntPtr ptrEmpty = new System.IntPtr();

                // we get length of strings
                Scilab_API_Wrapper.SciErr SciErr = Scilab_API_Wrapper.readNamedMatrixOfString(ptrEmpty, matrixName,
                                        &iRows, &iCols,
                                        lengthmatrixString, null);

                // we allocate each string
                matrixString = new string[iRows * iCols];
                for (int i = 0; i < iRows * iCols; i++)
                {
                    matrixString[i] = new string(' ', lengthmatrixString[i]);
                }

                // we get strings from scilab
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfString(ptrEmpty, matrixName,
                                                &iRows, &iCols,
                                                lengthmatrixString,
                                                matrixString);
            }
            return matrixString;
        }
        //=============================================================================
        /// <summary>
        /// check if named variable is a string matrix
        /// </summary>
        /// <param name="matrixName">variable name</param>
        /// <returns>true or false</returns>
        public Boolean isNamedStringType(String matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedStringType(ptrEmpty, matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_pstName"></param>
        /// <param name="_pstStrings"></param>
        /// <returns></returns>
        public Boolean createNamedSingleString(String _pstName, String _pstStrings)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedSingleString(ptrEmpty,
                _pstName,
                _pstStrings) == 0);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_pstName"></param>
        /// <returns></returns>
        public unsafe String getNamedSingleString(String matrixName)
        {
            string returnedString = null;

            int[] iDim = getNamedVarDimension(matrixName);

            if (iDim != null)
            {
                string[] matrixString = null;
                int iRows = iDim[0];
                int iCols = iDim[1];

                // we allocate lengthmatrixString
                int[] lengthmatrixString = new int[iRows * iCols];

                System.IntPtr ptrEmpty = new System.IntPtr();

                // we get length of strings
                Scilab_API_Wrapper.SciErr SciErr = Scilab_API_Wrapper.readNamedMatrixOfString(ptrEmpty, matrixName,
                                            &iRows, &iCols,
                                            lengthmatrixString, null);

                // we allocate each string
                matrixString = new string[iRows * iCols];
                for (int i = 0; i < iRows * iCols; i++)
                {
                    matrixString[i] = new string(' ', lengthmatrixString[i]);
                }

                // we get strings from scilab
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfString(ptrEmpty, matrixName,
                                                    &iRows, &iCols,
                                                    lengthmatrixString,
                                                    matrixString);
                if (SciErr.iErr == 0)
                {
                    returnedString = matrixString[0];
                }
            }
            return returnedString;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe int getNamedVarType(string matrixName)
        {
            int iType = 0;
            System.IntPtr ptrEmpty = new System.IntPtr();

            Scilab_API_Wrapper.SciErr SciErr = Scilab_API_Wrapper.getNamedVarType(ptrEmpty,
                matrixName, &iType);
            if (SciErr.iErr == 0) return iType;
            return 0;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedVarComplex(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedVarComplex(ptrEmpty, matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedVarMatrixType(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedVarMatrixType(ptrEmpty, matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean createNamedEmptyMatrix(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            int iVal = Scilab_API_Wrapper.createNamedEmptyMatrix(ptrEmpty, matrixName);
            if (iVal == 0) return true;
            return false;
        }
        //=============================================================================
        /// <summary>
        /// Write a named matrix of boolean in Scilab
        /// </summary>
        /// <param name="matrixName"> variable name</param>
        /// <param name="iRows"> Number of row</param>
        /// <param name="iCols"> Number of column</param>
        /// <param name="matrixBoolean"> pointer on data</param>
        /// <returns> if the operation successes (0) or not ( !0 )</returns>
        public int createNamedMatrixOfBoolean(string matrixName,
            int iRows, int iCols,
            Boolean[] matrixBoolean)
        {
            int[] matrixInt = new int[matrixBoolean.Length];
            for (int i = 0; i < matrixBoolean.Length; i++)
            {
                matrixInt[i] = matrixBoolean[i].CompareTo(true);
            }
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.createNamedMatrixOfBoolean(ptrEmpty,
                matrixName,
                iRows, iCols,
                matrixInt);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// Read a named matrix of boolean from Scilab
        /// </summary>
        /// <param name="matrixName"> variable name</param>
        /// <returns>a matrix of boolean from scilab. If Variable name does not exist returns null</returns>
        public unsafe Boolean[] readNamedMatrixOfBoolean(string matrixName)
        {
            Boolean[] matrixBoolean = null;
            int[] iDim = getNamedVarDimension(matrixName);

            if (iDim != null)
            {
                int iRows = iDim[0];
                int iCols = iDim[1];
                int[] matrixInt = new int[iRows * iCols];

                System.IntPtr ptrEmpty = new System.IntPtr();

                // get values in matrixDouble
                Scilab_API_Wrapper.SciErr SciErr = 
                    Scilab_API_Wrapper.readNamedMatrixOfBoolean(ptrEmpty, 
                    matrixName,
                    &iRows, &iCols,
                    matrixInt);

                if (matrixInt != null)
                {
                    matrixBoolean = new Boolean[iRows * iCols];
                    for (int i = 0; i < iRows * iCols; i++)
                    {
                        matrixBoolean[i] = (matrixInt[i] == 1);
                    }
                }
            }
            return matrixBoolean;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedBooleanType(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedBooleanType(ptrEmpty, matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean getNamedScalarBoolean(string matrixName)
        {
            Boolean returnedBoolvalue = false;
            Boolean[] bVals = readNamedMatrixOfBoolean(matrixName);
            if (bVals != null)
            {
                returnedBoolvalue = bVals[0];
            }
            return returnedBoolvalue;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="scalarBoolean"></param>
        /// <returns></returns>
        public Boolean createNamedScalarBoolean(string matrixName,
            Boolean scalarBoolean)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            int scalarInt = scalarBoolean.CompareTo(true);
            return (Scilab_API_Wrapper.createNamedScalarBoolean(ptrEmpty,
                matrixName,
                scalarInt) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedVarExist(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedVarExist(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedRowVector(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedRowVector(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedColumnVector(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedColumnVector(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedVector(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedVector(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedScalar(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedScalar(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedSquareMatrix(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedSquareMatrix(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedEmptyMatrix(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedEmptyMatrix(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedPointerType(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedPointerType(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixDouble"></param>
        /// <returns></returns>
        public int createNamedMatrixOfDouble(string matrixName, 
            int iRows, int iCols, 
            double[] matrixDouble)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr = 
                Scilab_API_Wrapper.createNamedMatrixOfDouble(ptrEmpty, 
                matrixName, 
                iRows, iCols, 
                matrixDouble);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixDoubleReal"></param>
        /// <param name="matrixDoubleImg"></param>
        /// <returns></returns>
        public int createNamedComplexMatrixOfDouble(string matrixName, 
            int iRows, int iCols, 
            double[] matrixDoubleReal, double[] matrixDoubleImg)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.createNamedComplexMatrixOfDouble(ptrEmpty,
                matrixName,
                iRows, iCols,
                matrixDoubleReal, matrixDoubleImg);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe double[] readNamedMatrixOfDouble(string matrixName)
        {
            int iRows = 0;
            int iCols = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.readNamedMatrixOfDouble(ptrEmpty, 
                matrixName, 
                &iRows, &iCols, 
                null);

            if (iRows * iCols > 0)
            {
                double[] matrixDouble = new double[iRows * iCols];

                // get values in matrixDouble
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfDouble(ptrEmpty, 
                    matrixName, 
                    &iRows, &iCols, 
                    matrixDouble);
                if (SciErr.iErr != 0) return null;
                return matrixDouble;
            }
            return null;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe double[] readNamedComplexMatrixOfDoubleRealPart(string matrixName)
        {
            double[] dRealPart = null;
            int[] iDim = getNamedVarDimension(matrixName);
            if (iDim != null)
            {
                int iRows = iDim[0];
                int iCols = iDim[1];

                double[] dImagPart = new double[iRows * iCols];
                dRealPart = new double[iRows * iCols];

                System.IntPtr ptrEmpty = new System.IntPtr();

                Scilab_API_Wrapper.SciErr SciErr =
                    Scilab_API_Wrapper.readNamedComplexMatrixOfDouble(ptrEmpty, 
                        matrixName,
                        &iRows, &iCols,
                        dRealPart,
                        dImagPart);
            }
            return dRealPart;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe double[] readNamedComplexMatrixOfDoubleImgPart(string matrixName)
        {
            double[] dImagPart = null;
            int[] iDim = getNamedVarDimension(matrixName);
            if (iDim != null)
            {
                int iRows = iDim[0];
                int iCols = iDim[1];

                double[] dRealPart = new double[iRows * iCols];
                dImagPart = new double[iRows * iCols];

                System.IntPtr ptrEmpty = new System.IntPtr();

                Scilab_API_Wrapper.SciErr SciErr =
                    Scilab_API_Wrapper.readNamedComplexMatrixOfDouble(ptrEmpty, 
                        matrixName,
                        &iRows, &iCols,
                        dRealPart,
                        dImagPart);
            }
            return dImagPart;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public Boolean isNamedDoubleType(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedDoubleType(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe double getNamedScalarDouble(string matrixName)
        {
            double returnedValue = double.NaN;
            if (isNamedVarMatrixType(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarDouble(ptrEmpty,
                    matrixName,
                    &returnedValue);
                if (iErr != 0) return double.NaN;
            }
            return returnedValue;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe double getNamedScalarComplexDoubleRealPart(string matrixName)
        {
            double dRealPart = double.NaN;
            double dImgPart = double.NaN;

            if (isNamedVarMatrixType(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarComplexDouble(ptrEmpty,
                                matrixName,
                                &dRealPart, &dImgPart);
                if (iErr != 0) return double.NaN;
            }
            return dRealPart;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe double getNamedScalarComplexDoubleImgPart(string matrixName)
        {
            double dRealPart = double.NaN;
            double dImgPart = double.NaN;

            if (isNamedVarMatrixType(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarComplexDouble(ptrEmpty,
                                matrixName,
                                &dRealPart, &dImgPart);
                if (iErr != 0) return double.NaN;
            }
            return dImgPart;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scalarName"></param>
        /// <param name="dblValue"></param>
        /// <returns></returns>
        public Boolean createNamedScalarDouble(string scalarName,
            double dblValue)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedScalarDouble(ptrEmpty,
                scalarName,
                dblValue) == 0);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scalarName"></param>
        /// <param name="dblRealPartValue"></param>
        /// <param name="dblImgPartValue"></param>
        /// <returns></returns>
        public Boolean createNamedScalarComplexDouble(string scalarName, 
            double dblRealPartValue, double dblImgPartValue)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedScalarComplexDouble(ptrEmpty,
                scalarName,
                dblRealPartValue, dblImgPartValue) == 0);
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixInt8"></param>
        /// <returns></returns>
        public int createNamedMatrixOfInteger8(string matrixName,
            int iRows, int iCols, 
            sbyte[] matrixInt8)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr = 
                Scilab_API_Wrapper.createNamedMatrixOfInteger8(ptrEmpty,
                matrixName,
                iRows, iCols, 
                matrixInt8);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixUInt8"></param>
        /// <returns></returns>
        public int createNamedMatrixOfUnsignedInteger8(string matrixName, 
            int iRows, int iCols, 
            byte [] matrixUInt8)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.createNamedMatrixOfUnsignedInteger8(ptrEmpty,
                matrixName,
                iRows, iCols,
                matrixUInt8);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixInt16"></param>
        /// <returns></returns>
        public int createNamedMatrixOfInteger16(string matrixName,
            int iRows, int iCols,
            Int16[] matrixInt16)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.createNamedMatrixOfInteger16(ptrEmpty,
                matrixName,
                iRows, iCols,
                matrixInt16);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixUInt16"></param>
        /// <returns></returns>
        public int createNamedMatrixOfUnsignedInteger16(string matrixName, 
            int iRows, int iCols,
            UInt16[] matrixUInt16)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.createNamedMatrixOfUnsignedInteger16(ptrEmpty,
                matrixName,
                iRows, iCols,
                matrixUInt16);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixInt32"></param>
        /// <returns></returns>
        public int createNamedMatrixOfInteger32(string matrixName, 
            int iRows, int iCols,
            Int32[] matrixInt32)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.createNamedMatrixOfInteger32(ptrEmpty,
                matrixName,
                iRows, iCols,
                matrixInt32);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <param name="iRows"></param>
        /// <param name="iCols"></param>
        /// <param name="matrixUInt32"></param>
        /// <returns></returns>
        public int createNamedMatrixOfUnsignedInteger32(
            string matrixName, 
            int iRows, int iCols,
            UInt32[] matrixUInt32)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.createNamedMatrixOfUnsignedInteger32(ptrEmpty,
                matrixName,
                iRows, iCols,
                matrixUInt32);
            return SciErr.iErr;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe int getNamedMatrixOfIntegerPrecision(string matrixName)
        {
            int iPrecision = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr = 
                Scilab_API_Wrapper.getNamedMatrixOfIntegerPrecision(ptrEmpty,
                matrixName,
                &iPrecision);
            if (SciErr.iErr != 0)
            {
                iPrecision = 0;
            }
            return iPrecision;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe sbyte[] readNamedMatrixOfInteger8(string matrixName)
        {
            int iRows = 0;
            int iCols = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.readNamedMatrixOfInteger8(ptrEmpty,
                matrixName,
                &iRows, &iCols,
                null);

            if (iRows * iCols > 0)
            {
                sbyte[] matrixSbyte = new sbyte[iRows * iCols];

                // get values in matrixSbyte
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfInteger8(ptrEmpty,
                    matrixName,
                    &iRows, &iCols,
                    matrixSbyte);
                if (SciErr.iErr != 0) return null;
                return matrixSbyte;
            }
            return null;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe byte[] readNamedMatrixOfUnsignedInteger8(string matrixName)
        {
            int iRows = 0;
            int iCols = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.readNamedMatrixOfUnsignedInteger8(ptrEmpty,
                matrixName,
                &iRows, &iCols,
                null);

            if (iRows * iCols > 0)
            {
                byte[] matrixbyte = new byte[iRows * iCols];

                // get values in matrixbyte
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfUnsignedInteger8(ptrEmpty,
                    matrixName,
                    &iRows, &iCols,
                    matrixbyte);
                if (SciErr.iErr != 0) return null;
                return matrixbyte;
            }
            return null;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe Int16[] readNamedMatrixOfInteger16(string matrixName)
        {
            int iRows = 0;
            int iCols = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.readNamedMatrixOfInteger16(ptrEmpty,
                matrixName,
                &iRows, &iCols,
                null);

            if (iRows * iCols > 0)
            {
                Int16[] matrixInt16 = new Int16[iRows * iCols];

                // get values in matrixInt16
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfInteger16(ptrEmpty,
                    matrixName,
                    &iRows, &iCols,
                    matrixInt16);
                if (SciErr.iErr != 0) return null;
                return matrixInt16;
            }
            return null;
        }
        //=============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixName"></param>
        /// <returns></returns>
        public unsafe UInt16[] readNamedMatrixOfUnsignedInteger16(string matrixName)
        {
            int iRows = 0;
            int iCols = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.readNamedMatrixOfUnsignedInteger16(ptrEmpty,
                matrixName,
                &iRows, &iCols,
                null);

            if (iRows * iCols > 0)
            {
                UInt16[] matrixUInt16 = new UInt16[iRows * iCols];

                // get values in matrixUInt16
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfUnsignedInteger16(ptrEmpty,
                    matrixName,
                    &iRows, &iCols,
                    matrixUInt16);
                if (SciErr.iErr != 0) return null;
                return matrixUInt16;
            }
            return null;
        }
        //=============================================================================
        public unsafe Int32[] readNamedMatrixOfInteger32(string matrixName)
        {
            int iRows = 0;
            int iCols = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.readNamedMatrixOfInteger32(ptrEmpty,
                matrixName,
                &iRows, &iCols,
                null);

            if (iRows * iCols > 0)
            {
                Int32[] matrixInt32 = new Int32[iRows * iCols];

                // get values in matrixInt32
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfInteger32(ptrEmpty,
                    matrixName,
                    &iRows, &iCols,
                    matrixInt32);
                if (SciErr.iErr != 0) return null;
                return matrixInt32;
            }
            return null;
        }
        //=============================================================================
        public unsafe UInt32[] readNamedMatrixOfUnsignedInteger32(string matrixName)
        {
            int iRows = 0;
            int iCols = 0;

            System.IntPtr ptrEmpty = new System.IntPtr();
            Scilab_API_Wrapper.SciErr SciErr =
                Scilab_API_Wrapper.readNamedMatrixOfUnsignedInteger32(ptrEmpty,
                matrixName,
                &iRows, &iCols,
                null);

            if (iRows * iCols > 0)
            {
                UInt32[] matrixUInt32 = new UInt32[iRows * iCols];

                // get values in matrixUInt32
                SciErr = Scilab_API_Wrapper.readNamedMatrixOfUnsignedInteger32(ptrEmpty,
                    matrixName,
                    &iRows, &iCols,
                    matrixUInt32);
                if (SciErr.iErr != 0) return null;
                return matrixUInt32;
            }
            return null;
        }
        //=============================================================================
        public Boolean isNamedIntegerType(string matrixName)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.isNamedIntegerType(ptrEmpty,
                matrixName) == 1);
        }
        //=============================================================================
        public unsafe sbyte getNamedScalarInteger8(string matrixName)
        {
            sbyte returnedValue = 0;
            if (isNamedVarExist(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarInteger8(ptrEmpty,
                    matrixName,
                    &returnedValue);
                if (iErr != 0) return returnedValue = 0;
            }
            return returnedValue;
        }
        //=============================================================================
        public unsafe Int16 getNamedScalarInteger16(string matrixName)
        {
            Int16 returnedValue = 0;
            if (isNamedVarExist(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarInteger16(ptrEmpty,
                    matrixName,
                    &returnedValue);
                if (iErr != 0) return returnedValue = 0;
            }
            return returnedValue;
        }
        //=============================================================================
        public unsafe Int32 getNamedScalarInteger32(string matrixName)
        {
            Int32 returnedValue = 0;
            if (isNamedVarExist(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarInteger32(ptrEmpty,
                    matrixName,
                    &returnedValue);
                if (iErr != 0) return returnedValue = 0;
            }
            return returnedValue;
        }
        //=============================================================================
        public unsafe byte getNamedScalarUnsignedInteger8(string matrixName)
        {
            byte returnedValue = 0;
            if (isNamedVarExist(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarUnsignedInteger8(ptrEmpty,
                    matrixName,
                    &returnedValue);
                if (iErr != 0) return returnedValue = 0;
            }
            return returnedValue;
        }
        //=============================================================================
        public unsafe UInt16 getNamedScalarUnsignedInteger16(string matrixName)
        {
            UInt16 returnedValue = 0;
            if (isNamedVarExist(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarUnsignedInteger16(ptrEmpty,
                    matrixName,
                    &returnedValue);
                if (iErr != 0) return returnedValue = 0;
            }
            return returnedValue;
        }
        //=============================================================================
        public unsafe UInt32 getNamedScalarUnsignedInteger32(string matrixName)
        {
            UInt32 returnedValue = 0;
            if (isNamedVarExist(matrixName) == true)
            {
                System.IntPtr ptrEmpty = new System.IntPtr();
                int iErr = Scilab_API_Wrapper.getNamedScalarUnsignedInteger32(ptrEmpty,
                    matrixName,
                    &returnedValue);
                if (iErr != 0) return returnedValue = 0;
            }
            return returnedValue;
        }
        //=============================================================================
        public Boolean createNamedScalarInteger8(string matrixName,
                        sbyte _cData)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedScalarInteger8(ptrEmpty,
                matrixName,
                _cData) == 0);
        }
        //=============================================================================
        public Boolean createNamedScalarInteger16(string matrixName,
                        Int16 _sData)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedScalarInteger16(ptrEmpty,
                matrixName,
                _sData) == 0);
        }
        //=============================================================================
        public Boolean createNamedScalarInteger32(string matrixName,
                        Int32 _iData)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedScalarInteger32(ptrEmpty,
                matrixName,
                _iData) == 0);
        }
        //=============================================================================
        public Boolean createNamedScalarUnsignedInteger8(string matrixName,
                        byte _ucData)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedScalarUnsignedInteger8(ptrEmpty,
                matrixName,
                _ucData) == 0);
        }
        //=============================================================================
        public Boolean createNamedScalarUnsignedInteger16(string matrixName,
                        UInt16 _usData)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedScalarUnsignedInteger16(ptrEmpty,
                matrixName,
                _usData) == 0);
        }
        //=============================================================================
        public Boolean createNamedScalarUnsignedInteger32(string matrixName,
                        UInt32 _uiData)
        {
            System.IntPtr ptrEmpty = new System.IntPtr();
            return (Scilab_API_Wrapper.createNamedScalarUnsignedInteger32(ptrEmpty,
                matrixName,
                _uiData) == 0);
        }
        //=============================================================================
    }
}
//=============================================================================
